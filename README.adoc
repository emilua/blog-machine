= Super blog machine 0.0.1

This is just a dirty script that I hacked to quickly get a functional
asciidoc-based blog.

It takes no arguments. Input is current working directory. It expects it to have
a structure similar to hugo (posts go in `content/post/*`). Then it creates
`public/` and writes output there.

It'll get post's date from the asciidoc attribute `revdate`. It uses standard
asciidoc syntax so you don't have to deal with special frontmatter bullshit.

You need to have Rouge installed for this script to work.

You also need to set env var `ASCIIDOCTORCSSPATH` to the path containing
`asciidoctor-default.css` (e.g.
`/usr/lib64/ruby/gems/3.1.0/gems/asciidoctor-2.0.18/data/stylesheets/asciidoctor-default.css`
on Gentoo).
