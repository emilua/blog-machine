local fs = require 'filesystem'
local system = require 'system'
local stream = require 'stream'
local regex = require 'regex'
local serial_port = require 'serial_port'
local pipe = require 'pipe'
local json = require 'protocol'

local DATEATTRREGEX = regex.new{
    pattern = '^:revdate: *([0-9]{4})-([0-9]{2})-([0-9]{2})',
    grammar = 'extended',
    optimize = true
}

local INPUT = fs.current_working_directory()
local OUTPUT = INPUT / 'public'
local CSSPATH = system.environment['ASCIIDOCTORCSSPATH']
local SKIPARCHIVE = system.environment['BLOG_MACHINE_SKIP_ARCHIVE'] == '1'

fs.create_directories(OUTPUT)

local function get_output_path(post_path)
    local scanner = stream.scanner.new()
    scanner.stream = serial_port.new()
    scanner.stream:assign(fs.open(post_path, {'read_only'}))
    while true do
        local line = scanner:get_line()
        local year, month, day = regex.match(DATEATTRREGEX, line)
        if year then
            year, month, day = tostring(year), tostring(month), tostring(day)
            return OUTPUT / year / month / day / post_path.stem / 'index.html'
        end
    end
end

-- TODO: should merge with previous function to generate a get_attrs() function
-- that works in 1-pass
local function get_title(post_path)
    local scanner = stream.scanner.new()
    scanner.stream = serial_port.new()
    scanner.stream:assign(fs.open(post_path, {'read_only'}))
    while true do
        local line = scanner:get_line()
        if line:starts_with('= ') then
            return tostring(line:sub(3))
        end
    end
end

if not CSSPATH then
    stream.write_all(
        system.err,
        'set env ASCIIDOCTORCSSPATH with path to asciidoctor-default.css\n')
    system.exit(1)
end

local json_conf

do
    json_conf = serial_port.new()
    json_conf:assign(fs.open(fs.path.new('config.json'), {'read_only'}))
    local buf = byte_span.new(fs.file_size(fs.path.new('config.json')))
    stream.read_all(json_conf, buf)
    json_conf = json.decode(tostring(buf))
end

local function generate_documents(document_type, document_extension, img_type, post, post_path, output)
    if SKIPARCHIVE then return end
    local p = system.spawn{
        program = document_type,
        arguments = {
            document_type,
            '--trace', '--verbose',
            '--base-dir', tostring(fs.current_working_directory() / 'content'),

            '--attribute', 'icons=font',
            '--attribute', 'icon-set=fas',

            '--require', 'asciidoctor-diagram',
            '--attribute', 'ditaa-format=' .. img_type,
            '--attribute', 'plantuml-format=' .. img_type,
            '--attribute', 'diagram-nocache-option',

            '--attribute', 'source-highlighter=rouge',

            '--out-file', tostring(output.parent_path .. document_extension),
            tostring(post_path)
        },
        environment = system.environment,
        stderr = 'share'
    }
    p:wait()
    if p.exit_code ~= 0 then
        stream.write_all(
            system.err, document_type .. 'returned ' .. p.exit_code .. '\n')
        system.exit(1)
    end
end

local cleanup_static = {}
scope_cleanup_push(function()
    for _, v in ipairs(cleanup_static) do
        fs.remove(v)
    end
end)

for file in fs.directory_iterator(INPUT / 'static') do
    fs.copy(file.path, OUTPUT, { existing = 'update' })
    local copy_in_input_dir = INPUT / 'content' / file.path.filename
    cleanup_static[#cleanup_static + 1] = copy_in_input_dir
    if not fs.exists(copy_in_input_dir) then
        fs.create_hardlink(file.path, copy_in_input_dir)
    end
end

local posts = {}

for post in fs.directory_iterator(INPUT / 'content' / 'post') do
    if post.path.extension ~= '.adoc' then
        goto continue
    end

    local output = get_output_path(post.path)
    local title = get_title(post.path)

    posts[#posts + 1] = {
        title = title,
        path = output.parent_path:lexically_relative(OUTPUT)
    }

    fs.create_directories(output.parent_path)

    local uptodate = false
    pcall(function()
        if fs.last_write_time(output) > fs.last_write_time(post.path) then
            uptodate = true
        end
    end)

    if uptodate then
        goto continue
    end

    local adout, wp = pipe.pair()
    wp = wp:release()
    local p = system.spawn{
        program = 'asciidoctor',
        arguments = {
            'asciidoctor',
            '--trace', '--verbose',
            '--base-dir', tostring(fs.current_working_directory() / 'content'),

            '--no-header-footer',
            '--attribute', 'nofooter',
            '--attribute', 'docinfo=shared',

            '--attribute', 'icons=font',
            '--attribute', 'icon-set=fas',

            '--require', 'asciidoctor-diagram',
            '--attribute', 'ditaa-format=svg',
            '--attribute', 'plantuml-format=svg',
            '--attribute', 'diagram-nocache-option',

            '--attribute', 'imagesdir=/',

            '--attribute', 'source-highlighter=rouge',

            '--attribute', 'sectlinks',
            '--attribute', 'sectanchors',
            '--attribute', 'figure-caption!',
            '--attribute', 'toc-title!',

            '--safe',

            '--out-file=-',

            tostring(post.path)
        },
        environment = system.environment,
        stdout = wp,
        stderr = 'share'
    }
    wp:close()
    wp = nil
    spawn(function()
        p:wait()
        if p.exit_code ~= 0 then
            stream.write_all(
                system.err, 'asciidoctor returned ' .. p.exit_code .. '\n')
            system.exit(1)
        end
    end):detach()

    local output_file = serial_port.new()
    output_file:assign(fs.open(
        output,
        {'write_only', 'create', 'truncate'},
        fs.mode(6, 6, 6)))

    stream.write_all(
        output_file, format([[
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="super blog machine 0.0.1">
<title>{title}</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic%7CNoto+Serif:400,400italic,700,700italic%7CDroid+Sans+Mono:400,700">
<link rel="alternate" type="application/feed+json" title="{blog_title}" href="{url}feed.json">
<style>
]],
        {'title', title}, {'blog_title', json_conf.title}, {'url', json_conf.baseURL}))

    do
        local css_file = serial_port.new()
        local buf = byte_span.new(4096)
        css_file:assign(fs.open(fs.path.new(CSSPATH), {'read_only'}))
        -- TODO: Emilua should have a stream_copy algorithm
        while true do
            local nread
            local ok = pcall(function() nread = css_file:read_some(buf) end)
            if not ok then break end
            stream.write_all(output_file, buf:first(nread))
        end
    end

    stream.write_all(
        output_file, format([[
</style>
<link rel="stylesheet" href="{url}syntax.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="article toc2 toc-left">
<div id="header">
<h1>{title}</h1>
<div id="toc" class="toc2">
<div id="toctitle">Menu</div>
<ul class="sectlevel1">
<li><a href="/">Home</a></li>
<li><a href="{url_pdf}">PDF</a></li>
<li><a href="{url_epub}">EPUB</a></li>
<li><a href="{feed_url}">JSON Feed</a></li>
</ul>
</div>
</div>
<div id="content">
]],
        {'title', title}, {'url', json_conf.baseURL},
        {'url_pdf', json_conf.baseURL .. tostring(output.parent_path:lexically_relative(OUTPUT)) .. '.pdf'},
        {'url_epub', json_conf.baseURL .. tostring(output.parent_path:lexically_relative(OUTPUT)) .. '.epub'},
        {'feed_url', json_conf.baseURL .. 'feed.json'}))

    do
        local buf = byte_span.new(4096)
        -- TODO: Emilua should have a stream_copy algorithm
        while true do
            local nread
            local ok = pcall(function() nread = adout:read_some(buf) end)
            if not ok then break end
            stream.write_all(output_file, buf:first(nread))
        end
    end

    stream.write_all(output_file, format([[
</div>
<div id="footer">
<div id="footer-text">
{footer}
</div>
</div>
</body>
</html>
]], {'footer', json_conf.footerText}))

    local dArgs = {{"asciidoctor-pdf", ".pdf", 'svg'}, {"asciidoctor-epub3", ".epub", 'png'}}

    for _, v in pairs(dArgs) do
        generate_documents(v[1], v[2], v[3], post, post.path, output)
    end

    ::continue::
end

local index = serial_port.new()
index:assign(fs.open(OUTPUT / 'index.html', {'write_only', 'create', 'truncate'}, fs.mode(6, 6, 6)))
stream.write_all(index, format([[
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="super blog machine 0.0.1">
<title>{title}</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic%7CNoto+Serif:400,400italic,700,700italic%7CDroid+Sans+Mono:400,700">
<link rel="alternate" type="application/feed+json" title="{title}" href="{url}feed.json">
<style>
]], {'title', json_conf.title}, {'url', json_conf.baseURL}))

do
    local css_file = serial_port.new()
    local buf = byte_span.new(4096)
    css_file:assign(fs.open(fs.path.new(CSSPATH), {'read_only'}))
    -- TODO: Emilua should have a stream_copy algorithm
    while true do
        local nread
        local ok = pcall(function() nread = css_file:read_some(buf) end)
        if not ok then break end
        stream.write_all(index, buf:first(nread))
    end
end

stream.write_all(index, format([[
</style>
</head>
<body class="article">
<div id="header">
<h1>{title}</h1>
</div>
<div id="content">
<ul>
]], {'title', json_conf.title}))

table.sort(posts, function(lhs, rhs) return lhs.path > rhs.path end)

local json_feed = { items = {} }

for _, v in ipairs(posts) do
    json_feed.items[#json_feed.items + 1] = {
        id = v.path:to_generic(),
        title = tostring(v.title),
        url = json_conf.baseURL .. v.path:to_generic() .. '/',
        attachments = {
            {
                url = json_conf.baseURL .. v.path:to_generic() .. '.pdf',
                mime_type = 'application/pdf',
                title = 'archive',
                size_in_bytes = (not SKIPARCHIVE) and fs.file_size(OUTPUT / v.path:to_generic() .. '.pdf') or nil
            },
            {
                url = json_conf.baseURL .. v.path:to_generic() .. '.epub',
                mime_type = 'application/epub+zip',
                title = 'archive',
                size_in_bytes = (not SKIPARCHIVE) and fs.file_size(OUTPUT / v.path:to_generic() .. '.epub') or nil
            }
        }
    }
    stream.write_all(
        index,
        format(
            '<li><a href="/{href}/">{title}</a></li>',
            {'href', v.path:to_generic()},
            {'title', tostring(v.title)}))
end

stream.write_all(index, format([[
</ul>
</div>
<div id="footer">
<div id="footer-text">
{footer}
</div>
</div>
</body>
</html>]], {'footer', json_conf.footerText}))

json_feed.version = 'https://jsonfeed.org/version/1.1'
json_feed.title = json_conf.title
json_feed.home_page_url = json_conf.baseURL
json_feed.language = json_conf.languageCode
json_feed.feed_url = json_conf.baseURL .. 'feed.json'
json_feed = json.encode(json_feed)

local feed_file = serial_port.new()
feed_file:assign(fs.open(OUTPUT / 'feed.json', {'write_only', 'create', 'truncate'}, fs.mode(6, 6, 6)))
stream.write_all(feed_file, json_feed)

for file in fs.directory_iterator(INPUT / 'content') do
    if file.path.extension == '.svg' then
        fs.rename(file.path,  OUTPUT / file.path.filename)
    end
end

system.spawn{
    program = 'rougify',
    arguments = {'rougify', 'style', 'github'},
    stdout = fs.open(fs.path.from_generic('public/syntax.css'), {'write_only', 'create', 'truncate'}, fs.mode(6, 6, 6)),
    stderr = 'share'
}:wait()
